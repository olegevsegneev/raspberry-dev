#!/usr/bin/python
from RPi import GPIO
from time import sleep

from pizypwm import *

ledPin = 11
led_pwm = PiZyPwm(100, ledPin, GPIO.BCM)
led_pwm.start(100)

power = 0
dim_up = range(0,80)
dim_up.reverse()
dim_down = range(0,80)

try:
    while True:
        for power in dim_up:
            led_pwm.changeDutyCycle(power)
            sleep(0.01)
        for power in dim_down:
            led_pwm.changeDutyCycle(power)
            sleep(0.01)

except KeyboardInterrupt:
    pass

except:
    raise

led_pwm.stop()
GPIO.cleanup()