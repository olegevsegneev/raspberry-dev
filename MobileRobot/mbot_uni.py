#!/usr/bin/python
import signal, os
import pygame
from RPi import GPIO
from MCP3008 import ADC
import time
import subprocess

from USonic import USonic
from Speaker import Speaker
from MovementControl import Walker

sonic_timeout = 0.2
sonic_time = 0
photo_timeout = 0.2
photo_time = 0
web_timeout = 0.5
web_time = 0
fire_timeout = 0.5
fire_time = 0

wall_maneuver = False
photo_thresh_1 = 900
photo_thresh_2 = 970

behav = 'wait'

GPIO.setmode(GPIO.BCM)

sonicPins = [ 8, 11 ] # trig, echo
walkerPins = [ 27, 22, 4, 17, 9 ] # 2-sleep
adcPins = [ 18, 3, 23, 24 ] # clk, cs, mosi, miso

sonic = USonic( *sonicPins )
walker = Walker( *walkerPins)
speaker = Speaker( pin_led_good = 14, pin_led_bad = 15, pin_buzz = 10 )
adc = ADC( *adcPins )

sp = 15 
tt = 1.0
wt = 2.0

patrol_path = [{'dir':(0,0), 'speed':(sp,sp), 'time':wt},
               {'dir':(0,0), 'speed':(sp,0), 'time':tt},
               {'dir':(0,0), 'speed':(sp,sp), 'time':wt},
               {'dir':(0,0), 'speed':(sp,0), 'time':tt},
               {'dir':(0,0), 'speed':(sp,sp), 'time':wt},
               {'dir':(0,0), 'speed':(sp,0), 'time':tt},
               {'dir':(0,0), 'speed':(sp,sp), 'time':wt},
              ]

def endProcess(signalnum = None, handler = None):
    walker.downSoft()
    speaker.say('die')
    GPIO.cleanup()
    exit(0)

def webControl():
    global web_time
    now = time.time()
    if now > web_time + web_timeout:
        web_time = now
        f = open('/home/pi/Robotics/control.txt')
        dir = f.read()
        f.close()
        if dir:
            walker.walkDir(dir)

def detectInterest():
    global sonic_time
    now = time.time()
    if now > sonic_time + sonic_timeout:
        sonic_time = now
        dist = sonic.getDistance(5)
        #print dist
        if dist < 5:
            walker.walkDir('stop')
            speaker.say('attack')
        elif dist < 50:
            walker.walkDir('forward')
            speaker.say('attack')
        else:      
            walker.walkDir('left')

def detectLine():
    global photo_time
    now = time.time()
    if now > photo_time + photo_timeout:
        photo_time = now
        l1 = adc.read(0)
        l2 = adc.read(1)
        #print '==>', l1, l2
        if l1 > photo_thresh_1 and l2 > photo_thresh_2:
            walker.setSpeed(20)
            walker.walkDir('backward')
        elif l1 > photo_thresh_1:
            walker.setSpeed(20)
            walker.walkDir('right')
        elif l2 > photo_thresh_2:
            walker.setSpeed(20)
            walker.walkDir('left')
        else:
            walker.setSpeed(30)
            walker.walkDir('forward')

def detectWall():
    global sonic_time, wall_maneuver
    now = time.time()
    if now > sonic_time + sonic_timeout:
        sonic_time = now
        dist = sonic.getDistance(5)
        #print dist
        if dist < 15:
            walker.walkDir('left')
            if not wall_maneuver:
                walker.downHard()
                time.sleep(0.01)
                walker.setSpeed(40)
            else:
                walker.setSpeed(30)
            speaker.say('wall')
            wall_maneuver = True
        elif dist < 25:
            if not wall_maneuver:
                walker.setSpeed(30)
                speaker.say('wall')
        else:
            walker.walkDir('forward')
            if wall_maneuver:
                walker.setSpeed(40)
            else:
                walker.setSpeed(30)
            wall_maneuver = False
          

def detectFire():
    f1 = adc.read(0)
    f2 = adc.read(1)
    print 'fire==>', f1, f2

def eHandle():
    global behav
    pygame.event.pump()

    for e in pygame.event.get():
        if e.type==pygame.KEYDOWN:
            if e.key == pygame.K_w:
                walker.walkDir('forward')
            elif e.key == pygame.K_s:
                walker.walkDir('backward')
            elif e.key == pygame.K_a:
                walker.walkDir('left')
            elif e.key == pygame.K_d:
                walker.walkDir('right')
            elif e.key == pygame.K_q:
                walker.walkDir('stop')
        elif e.type == pygame.KEYUP:
            if e.key == pygame.K_p:
                speaker.say('start')
                walker.walkPath( path )
                speaker.say('stop')
            elif e.key == pygame.K_o:
                speaker.say('laugh')
                behav = 'wall'
                walker.setSpeed(30)
                walker.walkDir('forward')
            elif e.key == pygame.K_l:
                speaker.say('laugh')
                behav = 'line'
                walker.setSpeed(30)
                walker.walkDir('forward')
            elif e.key == pygame.K_f:
                speaker.say('laugh')
                behav = 'follow'
                walker.setSpeed(30)
                walker.walkDir('stop')
            elif e.key == pygame.K_r:
                walker.walkDir('stop')
                walker.setSpeed(30)
                speaker.say('shout')
                behav = 'wait'
            elif e.key == pygame.K_i:
                speaker.say('laugh')
                subprocess.Popen(['python','/home/pi/Robotics/MiniHTTP/server.py'])
                behav = 'web'
            elif e.key == pygame.K_x:
                endProcess()
            else:
                walker.walkDir('stop')


signal.signal(signal.SIGTERM, endProcess)
signal.signal(signal.SIGINT, endProcess)

pygame.init()
pygame.display.set_mode((320,200))
time.sleep(5)
speaker.say('born')

walker.setSpeed(30)
walker.setEHandler(eHandle)

try:
    while True:
        eHandle()
        if behav=='wall':
            detectWall()
        elif behav=='line':
            detectLine()
        elif behav=='follow':
            detectInterest()
        elif behav == 'web':
            webControl()
        walker.pump()
        #now = time.time()
        #if now > fire_time + fire_timeout:
        #    fire_time = now
        #    detectFire()

except KeyboardInterrupt:
    pass
except:
    raise

endProcess()
