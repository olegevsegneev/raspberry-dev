import RPi.GPIO as GPIO

class ADC:
    def __init__( self, pin_clk, pin_cs, pin_mosi, pin_miso ):
        self.pin_clk = pin_clk
        self.pin_cs = pin_cs
        self.pin_mosi = pin_mosi
        self.pin_miso = pin_miso
        GPIO.setup( pin_clk, GPIO.OUT )
        GPIO.setup( pin_cs, GPIO.OUT )
        GPIO.setup( pin_mosi, GPIO.OUT )
        GPIO.setup( pin_miso, GPIO.IN )
    
    def read( self, adcnum ):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1

        GPIO.output( self.pin_cs, True )

        GPIO.output( self.pin_clk, False )  # start clock low
        GPIO.output( self.pin_cs, False )     # bring CS low

        commandout = adcnum
        commandout |= 0x18  # start bit + single-ended bit
        commandout <<= 3    # we only need to send 5 bits here
        for i in range(5):
            if( commandout & 0x80 ):
                    GPIO.output( self.pin_mosi, True )
            else:
                    GPIO.output( self.pin_mosi, False )
            commandout <<= 1
            GPIO.output( self.pin_clk, True )
            GPIO.output( self.pin_clk, False )

        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
            GPIO.output( self.pin_clk, True )
            GPIO.output( self.pin_clk, False )
            adcout <<= 1
            if (GPIO.input( self.pin_miso )):
                adcout |= 0x1

        GPIO.output( self.pin_cs, True )
        
        adcout >>= 1       # first bit is 'null' so drop it
        return adcout
