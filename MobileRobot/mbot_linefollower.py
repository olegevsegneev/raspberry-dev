#!/usr/bin/python
import signal, os
import pygame
from RPi import GPIO
from MCP3008 import ADC
import time
import subprocess

from Speaker import Speaker
from MovementControl import Walker

PHOTO_TIMEOUT = 0.2
photo_time = 0

PHOTO_THRESH_1 = 900
PHOTO_THRESH_2 = 970

behav = 'wait'

GPIO.setmode(GPIO.BCM)

walkerPins = [ 27, 22, 4, 17, 9 ] # 9-sleep
adcPins = [ 18, 3, 23, 24 ] # clk, cs, mosi, miso

walker = Walker( *walkerPins)
speaker = Speaker( pin_led_good = 14, pin_led_bad = 15, pin_buzz = 10 )
adc = ADC( *adcPins )

def endProcess(signalnum = None, handler = None):
    walker.downSoft()
    speaker.say('die')
    GPIO.cleanup()
    exit(0)

def detectLine():
    global photo_time
    now = time.time()
    if now > photo_time + PHOTO_TIMEOUT:
        photo_time = now
        l1 = adc.read(0)
        l2 = adc.read(1)
        #print '==>', l1, l2
        if l1 > PHOTO_THRESH_1 and l2 > PHOTO_THRESH_2:
            walker.setSpeed(20)
            walker.walkDir('backward')
        elif l1 > PHOTO_THRESH_1:
            walker.setSpeed(20)
            walker.walkDir('right')
        elif l2 > PHOTO_THRESH_2:
            walker.setSpeed(20)
            walker.walkDir('left')
        else:
            walker.setSpeed(30)
            walker.walkDir('forward')

def eHandle():
    global behav
    pygame.event.pump()

    for e in pygame.event.get():
        if e.type==pygame.KEYDOWN:
            if e.key == pygame.K_w:
                walker.walkDir('forward')
            elif e.key == pygame.K_s:
                walker.walkDir('backward')
            elif e.key == pygame.K_a:
                walker.walkDir('left')
            elif e.key == pygame.K_d:
                walker.walkDir('right')
            elif e.key == pygame.K_q:
                walker.walkDir('stop')
        elif e.type == pygame.KEYUP:
            if e.key == pygame.K_l:
                speaker.say('laugh')
                behav = 'line'
                walker.setSpeed(30)
                walker.walkDir('forward')
            elif e.key == pygame.K_r:
                walker.walkDir('stop')
                walker.setSpeed(30)
                speaker.say('shout')
                behav = 'wait'
            elif e.key == pygame.K_x:
                endProcess()
            else:
                walker.walkDir('stop')


signal.signal(signal.SIGTERM, endProcess)
signal.signal(signal.SIGINT, endProcess)

pygame.init()
pygame.display.set_mode((320,200))
time.sleep(5)
speaker.say('born')

walker.setSpeed(30)

try:
    while True:
        eHandle()
        if behav=='line':
            detectLine()
        walker.pump()

except KeyboardInterrupt:
    pass
except:
    raise

endProcess()
