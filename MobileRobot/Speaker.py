# Speaker class
import time
import RPi.GPIO as GPIO

phrases = {
    'start': [{'src':['buzz','good'], 'time':1, 'repeat':1},
              {'src':[], 'time':0.5, 'repeat':1},
              {'src':['buzz','good'], 'time':0.2, 'repeat':2},
             ],
    'stop':  [{'src':['buzz','good'], 'time':0.2, 'repeat':2},
              {'src':[], 'time':0.5, 'repeat':1},
              {'src':['buzz','good'], 'time':1, 'repeat':1},
             ],
    'attack':  [{'src':['buzz','bad'], 'time':0.05, 'repeat':2}],
    'wall':  [{'src':['buzz','good'], 'time':0.05, 'repeat':2}],
    'shout': [{'src':['buzz','good'], 'time':1, 'repeat':1}],
    'laugh': [{'src':['buzz','good'], 'time':0.05, 'repeat':5}],
    'die':   [{'src':['buzz','good','bad'], 'time':0.2, 'repeat':3},
              {'src':['buzz','good','bad'], 'time':1, 'repeat':1}
             ],
    'born':  [{'src':['buzz','good'], 'time':0.2, 'repeat':5}],
}

class Speaker:
    _pins = {}#{'buzz':None, 'good':None, 'bad':None}

    def __init__( self, pin_led_good=None, pin_led_bad=None, pin_buzz=None ):
        if pin_led_good:
            self._pins['good'] = pin_led_good
            GPIO.setup( pin_led_good, GPIO.OUT )
            GPIO.output( pin_led_good, True )
        if pin_led_bad:
            self._pins['bad'] = pin_led_bad
            GPIO.setup( pin_led_bad, GPIO.OUT )
            GPIO.output( pin_led_bad, True )
        if pin_buzz:
            self._pins['buzz'] = pin_buzz
            GPIO.setup( pin_buzz, GPIO.OUT )
            GPIO.output( pin_buzz, True )

    def say( self, phrase_id ):
        phrase = phrases[phrase_id]
        for word in phrase:
            for i in range( word['repeat'] ):
                # on
                for src in word['src']:
                    if self._pins.has_key(src):
                        GPIO.output(self._pins[src], False)
                # pause
                time.sleep( word['time'] )
                # off
                for src in word['src']:
                    if self._pins.has_key(src):
                        GPIO.output(self._pins[src], True)
                # pause
                time.sleep( word['time'] )

