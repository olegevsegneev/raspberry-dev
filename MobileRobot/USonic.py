import time
import RPi.GPIO as GPIO

class USonic:

    def __init__( self, pin_trig, pin_echo ):
        self.pin_trig = pin_trig
        self.pin_echo = pin_echo

        GPIO.setup( self.pin_trig, GPIO.OUT )
        GPIO.setup( self.pin_echo, GPIO.IN )

        # Set trigger to False (Low)
        GPIO.output( self.pin_trig, False )

        # Allow module to settle
        time.sleep(0.5)

    def getDistance( self, count ):
        ds = []
        for idx in range(0,count):
            ds.append( self._getDistance() )
            time.sleep(0.01)
        ds.sort()
        middle = count/2
        return ds[middle]

    def _waitLevel( self, pin, level, timeout ):
        start = time.time()
        done = False
        while not done:
            now = time.time()
            delta = now - start
            if delta > timeout:
                done = True
            if GPIO.input( self.pin_echo ) == level:
                done = True
        return delta

    def _getDistance( self ):
        GPIO.output( self.pin_trig, True )
        time.sleep(0.000010)
        GPIO.output(self.pin_trig, False)
        self._waitLevel( self.pin_echo, 1, 0.005 )

        if GPIO.input( self.pin_echo ) == 1:
            delta = self._waitLevel( self.pin_echo, 0, 0.03 )
            return delta * 34300 / 2
        else:
            return 1000
