#!/usr/bin/python
import signal, os
import pygame
from RPi import GPIO
import time
import subprocess

from USonic import USonic
from Speaker import Speaker
from MovementControl import Walker

SONIC_TIMEOUT = 0.2
sonic_time = 0

wall_maneuver = False

behav = 'wait'

GPIO.setmode(GPIO.BCM)

sonicPins = [ 8, 11 ] # trig, echo
walkerPins = [ 27, 22, 4, 17, 9 ] # 9-sleep

sonic = USonic( *sonicPins )
walker = Walker( *walkerPins)
speaker = Speaker( pin_led_good = 14, pin_led_bad = 15, pin_buzz = 10 )

def endProcess(signalnum = None, handler = None):
    walker.downSoft()
    speaker.say('die')
    GPIO.cleanup()
    exit(0)

def detectWall():
    global sonic_time, wall_maneuver
    now = time.time()
    if now > sonic_time + SONIC_TIMEOUT:
        sonic_time = now
        dist = sonic.getDistance(5)
        #print dist
        if dist < 15:
            walker.walkDir('left')
            if not wall_maneuver:
                walker.downHard()
                time.sleep(0.01)
                walker.setSpeed(40)
            else:
                walker.setSpeed(30)
            speaker.say('wall')
            wall_maneuver = True
        elif dist < 25:
            if not wall_maneuver:
                walker.setSpeed(30)
                speaker.say('wall')
        else:
            walker.walkDir('forward')
            if wall_maneuver:
                walker.setSpeed(40)
            else:
                walker.setSpeed(30)
            wall_maneuver = False
          
def eHandle():
    global behav
    pygame.event.pump()

    for e in pygame.event.get():
        if e.type==pygame.KEYDOWN:
            if e.key == pygame.K_w:
                walker.walkDir('forward')
            elif e.key == pygame.K_s:
                walker.walkDir('backward')
            elif e.key == pygame.K_a:
                walker.walkDir('left')
            elif e.key == pygame.K_d:
                walker.walkDir('right')
            elif e.key == pygame.K_q:
                walker.walkDir('stop')
        elif e.type == pygame.KEYUP:
            if e.key == pygame.K_o:
                speaker.say('laugh')
                behav = 'wall'
                walker.setSpeed(30)
                walker.walkDir('forward')
            elif e.key == pygame.K_r:
                walker.walkDir('stop')
                walker.setSpeed(30)
                speaker.say('shout')
                behav = 'wait'
            elif e.key == pygame.K_x:
                endProcess()
            else:
                walker.walkDir('stop')

signal.signal(signal.SIGTERM, endProcess)
signal.signal(signal.SIGINT, endProcess)

pygame.init()
pygame.display.set_mode((320,200))
time.sleep(5)
speaker.say('born')

walker.setSpeed(30)

try:
    while True:
        eHandle()
        if behav=='wall':
            detectWall()
        walker.pump()

except KeyboardInterrupt:
    pass
except:
    raise

endProcess()
