# Movement controller class

from RPi import GPIO
import time
import os

DEBUG = 1

SPEEDUP_TIMEOUT = 2

channel_map = {
4:0, 17:1, 27:2, 22:3,
#4:0, 17:1, 18:2, 27:3,
#22:4, 23:5, 24:6, 25:7,
}

class PWM:
    def __init__( self, pin ):
        self.pin = pin

    def set( self, value ):
        cmd = 'echo "%d=%.2f" > /dev/pi-blaster' % ( channel_map[self.pin], value/100. )
        #print cmd
        os.system(cmd)
        #sleep(0.01)
    

class Walker:
    speed = 0
    dir = 'stop'
    ehandler = None
    speedup = False
    speedup_time = 0

    def __init__( self, l1, l2, r1, r2, slp = None ):
        self.left = [PWM(l1), PWM(l2)]
        self.right = [PWM(r1), PWM(r2)]

        self.downSoft()

        if slp:
            GPIO.setup( slp, GPIO.OUT )
            GPIO.output( slp, True )

    def pump( self ):
        now = time.time()
        if self.speedup and now > self.speedup_time + SPEEDUP_TIMEOUT:
            self.setSpeed( self.speedup )

    def setSpeed( self, speed, from_speed=None, force=False ):
        if self.speed == speed and not force:
            return
        #if self.speed - speed >= 20:
        #    self.downHard()
        #    sleep(0.1)
        if from_speed:
            self.speed = from_speed
            self.speedup = speed
            self.speedup_time = time.time()
        else:
            self.speed = speed
            self.speedup = False
        if DEBUG: print 'speed==>', self.speed
        self.walkDir(force=True)

    def setEHandler( self, ehandler ):
        self.ehandler = ehandler

    def downSoft( self ):
        self.left[1].set(0)
        self.left[0].set(0)
        self.right[1].set(0)
        self.right[0].set(0)
        if DEBUG: print 'down soft'

    def downHard( self ):
        self.left[1].set(100)
        self.left[0].set(100)
        self.right[1].set(100)
        self.right[0].set(100)
        if DEBUG: print 'down hard'

    def walkPath( self, path ):
        for step in path:
            if step['dir'][0]:
                self.left[0].set(step['speed'][0])
                self.left[1].set(0)
            else:
                self.left[1].set(step['speed'][0])
                self.left[0].set(0)

            if step['dir'][1]:
                self.right[0].set(step['speed'][1])
                self.right[1].set(0)
            else:
                self.right[1].set(step['speed'][1])
                self.right[0].set(0)

            if self.ehandler:
                self.ehandler()
            time.sleep(step['time'])

        self.dir = 'stop'
        self.downSoft()

    def walkDir( self, dir=None, force=False ):
        if self.dir == dir and not force:
            return

        if dir is not None:
            self.dir = dir

        if DEBUG: print 'dir==>', self.dir

        if self.dir == 'left':
            self.walkLeft()
        elif self.dir == 'right':
            self.walkRight()
        elif self.dir == 'forward':
            self.walkForward()
        elif self.dir == 'backward':
            self.walkBackward()
        elif self.dir == 'stop':
            self.downSoft()

    def walkLeft( self ):
        self.left[1].set(self.speed)
        self.left[0].set(0)
        self.right[1].set(0)
        self.right[0].set(0)

    def walkRight( self ):
        self.left[1].set(0)
        self.left[0].set(0)
        self.right[1].set(self.speed)
        self.right[0].set(0)

    def walkForward( self ):
        self.left[1].set(self.speed)
        self.left[0].set(0)
        self.right[1].set(self.speed)
        self.right[0].set(0)

    def walkBackward( self ):
        self.left[1].set(0)
        self.left[0].set(self.speed)
        self.right[1].set(0)
        self.right[0].set(self.speed)
