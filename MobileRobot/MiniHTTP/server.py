#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from os import curdir, sep
#from urlparse import urlparse
import cgi
import time
import subprocess


PORT_NUMBER = 8000
folder = '/home/pi/Robotics/'
web_folder = '/home/pi/Robotics/MiniHTTP'
#folder = ''
server_alive = False

class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        global server_alive
        if self.path=="/":
            self.path="/control.html"
        try:
            sendReply = False

            if self.path == "/tele":
                f = open(folder+'tele.txt','r')
                data = f.read()
                f.close()

                self.send_response(200)
                self.send_header('Content-type', 'text/plain')
                self.end_headers()
                self.wfile.write(data)
                return

            elif self.path.startswith("/camera_init"):
                cmd = '/home/pi/mjpg-streamer/mjpg-streamer.sh'
                subprocess.Popen( [cmd, 'start'] )
 
                self.send_response(200)
                self.send_header("Content-type", "text/html")
                self.end_headers()
                self.wfile.write("started")          
                return

            elif self.path.startswith("/move"):
                prefix, dir =  self.path.split('_')
                f = open(folder+'control.txt','w')
                f.write(dir)
                f.close()

                self.send_response(200)
                self.send_header('Content-type', 'text/plain')
                self.end_headers()
                self.wfile.write('ok')
                return

            elif self.path.endswith(".html"):
                mimetype='text/html'
                sendReply = True
            elif self.path.endswith(".png"):
                mimetype='image/png'
                sendReply = True
            elif self.path.endswith(".js"):
                mimetype='application/javascript'
                sendReply = True
            elif self.path.endswith(".css"):
                mimetype='text/css'
                sendReply = True
            elif self.path == "/stop":
                server_alive = False
            
            if sendReply == True:
                f = open(web_folder + sep + self.path, 'rb') 
                self.send_response(200)
                self.send_header('Content-type',mimetype)
                self.end_headers()
                self.wfile.write(f.read())
                f.close()
                return

        except IOError:
            self.send_error(404,'File Not Found: %s' % self.path)

try:
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ' , PORT_NUMBER
    server_alive = True
    while server_alive:
        server.handle_request()
    #server.serve_forever()
    
except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    cmd = '/home/pi/mjpg-streamer/mjpg-streamer.sh'
    subprocess.Popen([cmd, 'stop'])

server.socket.close()
