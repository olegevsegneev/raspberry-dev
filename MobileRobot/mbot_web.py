#!/usr/bin/python
import signal, os
from RPi import GPIO
import time
import subprocess

from Speaker import Speaker
from MovementControl import Walker

CTRL_PATH = '/home/pi/Robotics/'
WEB_TIMEOUT = 0.5
web_time = 0

GPIO.setmode(GPIO.BCM)

walkerPins = [ 27, 22, 4, 17, 9 ] # 9-sleep

walker = Walker( *walkerPins)
speaker = Speaker( pin_led_good = 14, pin_led_bad = 15, pin_buzz = 2 )

def end_process(signalnum = None, handler = None):
    walker.downSoft()
    speaker.say('die')
    GPIO.cleanup()
    exit(0)

def web_control():
    global web_time
    now = time.time()
    if now > web_time + WEB_TIMEOUT:
        web_time = now
        f = open(CTRL_PATH+'control.txt')
        dir = f.read()
        f.close()
        if dir:
            walker.walkDir(dir)

def run_web():
    speaker.say('laugh')
    subprocess.Popen(['python',CTRL_PATH+'MiniHTTP/server.py'])

signal.signal(signal.SIGTERM, end_process)
signal.signal(signal.SIGINT, end_process)

time.sleep(5)
speaker.say('born')

walker.setSpeed(30)

run_web()

try:
    while True:
        web_control()

except KeyboardInterrupt:
    pass
except:
    raise

end_process()
