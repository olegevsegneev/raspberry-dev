import sqlite3
import time
from RPi import GPIO

sensor_timeout = 1.
sensor_time = 0
actuator_timeout = 1.
actuator_time = 0

DEBUG = 0

dbs = {'sensors':'home_inform',
       'actuators':'home_lampa',
       'common':'home_flat'}

sensors = [
{'id':'S1','type':'temp','path':['local', 17]},
{'id':'S2','type':'temp','path':['tele', 'A2']},
]

actuators = [
{'id':'A1','type':'relay','path':['local',27], 'default': False},
{'id':'A2','type':'relay','path':['tele','A2'], 'default': False},
]

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

class DBController:
    def __init__(self, db_path):
        self.conn = sqlite3.connect(db_path)

    def execute( self, query ):
        self.conn.row_factory = dict_factory
        c = self.conn.cursor()
        c.execute( query )
        return c.fetchall()

    def listActuatorStates( self ):
        q = 'select * from %s' % dbs['actuators']
        states = {}
        for row in self.execute(q):
            states[row['number'].encode('ascii')] = row['status']
        return states

    def refreshSensorValues( self, kw ):
        for k,v in kw:
            q = 'update %s set value=%s where code=%s' % (dbs['sensors',v,k])

class AHController:
    ssates = {}
    astates = {}

    def __init__( self, dbctrl ):
        self.db = dbctrl
        for a in actuators:
            self.astates[a['id']] = a['default']

    def refreshSensors( self ):
        for s in sensors:
            self.sendSensorRequest( self )       

    def refreshActuators( self ):
        states = self.db.listActuatorStates()
        for a in actuators:
            if a['path'][0] == 'local':
                self.triggerLocal(a, states[a['id']])
            elif a['type'][0] == 'tele':
                self.triggerTele(a, states[a['id']])

    def triggerLocal( self, a, state ):
        if self.astates[a['id']] == state:
            if DEBUG: print 'already in state', a['id'], state
            return
        if a['type'] == 'relay':
            if DEBUG: print 'set state', a['id'], state
            GPIO.output( a['path'][1], state )
        self.astates[a['id']] = state

dbctrl = DBController( '/home/pi/rh.db' )
ahctrl = AHController( dbctrl )

#q = "SELECT * FROM home_roz;"
#print dbctrl.execute(q)

GPIO.setmode(GPIO.BCM)
GPIO.setup( 27, GPIO.OUT )
GPIO.output( 27, 0 )


try:
    while 1:
        now = time.time()
        if sensor_time + sensor_timeout < now:
            sensor_time = now
            #ahctrl.refreshSensors()

        now = time.time()
        if actuator_time + actuator_timeout < now:
            actuator_time = now
            ahctrl.refreshActuators()
                  
except KeyboardInterrupt:
    pass

GPIO.cleanup()
